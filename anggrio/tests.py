from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import homepage, confirmationPage
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class StoryUnitTest(TestCase):
    def setUp(self):
        Status.objects.create(name = 'test name', status = 'test for new object')

    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_homepage_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_is_completed(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, how are you?', html_response)

    def test_homepage_receive_post_request(self):
        response = Client().post('/', {'name': 'dummy', 'status': 'dummy'})
        self.assertEqual(response.status_code, 200)

    def test_homepage_receive_non_post_request(self):
        Client().get('/', {'status': 'dummy'})
        response = Status.objects.all().count()
        self.assertEqual(response, 1)

    def test_status_model_add_object(self):
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def test_status_model_str_func(self):
        check = Status.objects.get(pk=1)
        self.assertEqual(f'{check}', 'test for new object')

    def test_status_model_status_field_label(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('status').verbose_name
        self.assertEqual(length, 'status')

    def test_status_model_status_field_length(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('status').max_length
        self.assertEqual(length, 255)

    def test_status_form_is_functioning(self):
        form = StatusForm(data = {'name': 'name', 'status': 'status'})
        self.assertTrue(form.is_valid())

    def test_status_model_name_field_label(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('name').verbose_name
        self.assertEqual(length, 'name')
    
    def test_status_model_name_field_length(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('name').max_length
        self.assertEqual(length, 255)

    def test_confirmation_page_url_is_exist(self):
        response = Client().get('/confirm')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_page_using_confirmationPage_func(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirmationPage)

    def test_confirmation_page_using_confirmationPage_template(self):
        response = Client().get('/confirm')
        self.assertTemplateUsed(response, 'confirmationPage.html')

    def test_confirmation_page_is_completed(self):
        request = HttpRequest()
        response = confirmationPage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Please confirm your name and status', html_response)

    def test_confirmation_page_receive_post_request_for_confirm(self):
        response = Client().post('/confirm', {'confirm': 'Submit Status', 'name': 'dummy', 'status': 'dummy'})
        html_response = response.content.decode('utf8')
        self.assertIn('dummy', html_response)
    
    def test_confirmation_page_receive_post_request_for_continue(self):
        Client().post('/confirm', {'continue': 'Submit Status', 'name': 'dummy', 'status': 'dummy'})
        response = Status.objects.all().count()
        self.assertEqual(response, 2)
    
    def test_confirmation_page_receive_post_request_for_go_back(self):
        response = Client().post('/confirm', {'back': 'Submit', 'name': 'dummy', 'status': 'dummy'})
        response = Status.objects.all().count()
        self.assertEqual(response, 1)

class StoryFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_can_submit_a_name_and_status(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Status Page", self.browser.title)
        time.sleep(5)
        name = self.browser.find_element_by_id('id_name')
        name.send_keys("Anggrio Wildanhadi Sutopo")
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Test status")
        submit = self.browser.find_element_by_name('confirm')
        time.sleep(5)
        submit.click()
        time.sleep(5)
        self.assertIn("Confirmation Page", self.browser.title)
        self.assertIn("Anggrio Wildanhadi Sutopo", self.browser.page_source)
        self.assertIn("Test status", self.browser.page_source)
        submit = self.browser.find_element_by_name('continue')
        time.sleep(5)
        submit.click()
        time.sleep(5)
        self.assertIn("Anggrio Wildanhadi Sutopo", self.browser.page_source)
        self.assertIn("Test status", self.browser.page_source)

    def test_can_go_back_to_submission_form(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Status Page", self.browser.title)
        time.sleep(5)
        name = self.browser.find_element_by_id('id_name')
        name.send_keys("Anggrio Wildanhadi Sutopo")
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Test status")
        submit = self.browser.find_element_by_name('confirm')
        time.sleep(5)
        submit.click()
        time.sleep(5)
        self.assertIn("Confirmation Page", self.browser.title)
        self.assertIn("Anggrio Wildanhadi Sutopo", self.browser.page_source)
        self.assertIn("Test status", self.browser.page_source)
        submit = self.browser.find_element_by_id('back')
        time.sleep(5)
        submit.click()
        self.assertIn("Status", self.browser.page_source)

    def test_can_change_colors_of_status_list(self):
        self.browser.get('http://localhost:8000')
        change = self.browser.find_element_by_id('change')
        bgcolor = self.browser.find_element_by_tag_name("table").value_of_css_property("background-color")
        color = self.browser.find_element_by_tag_name("table").value_of_css_property("color")
        self.assertEqual("rgba(52, 58, 64, 1)", bgcolor)
        self.assertEqual("rgba(255, 255, 255, 1)", color)
        time.sleep(5)
        change.click()
        bgcolor = self.browser.find_element_by_tag_name("table").value_of_css_property("background-color")
        color = self.browser.find_element_by_tag_name("table").value_of_css_property("color")
        self.assertEqual("rgba(137, 207, 240, 1)", bgcolor)
        self.assertEqual("rgba(0, 0, 255, 1)", color)
        time.sleep(5)
        change.click()
        bgcolor = self.browser.find_element_by_tag_name("table").value_of_css_property("background-color")
        color = self.browser.find_element_by_tag_name("table").value_of_css_property("color")
        self.assertEqual("rgba(52, 58, 64, 1)", bgcolor)
        self.assertEqual("rgba(255, 255, 255, 1)", color)